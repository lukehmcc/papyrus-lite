package main

// a file with all the basic utilities that I may need

import (
  "strings"
  "log"
  // "fmt"
)

// grabs file name(of a directory or a file)
func fileName(fullPath string) string {
  if(fullPath == ""){
    return ""
  }
  return fullPath[1+strings.LastIndex(fullPath[:len(fullPath)-1], "/"):]
}

// grabs the parent directory
func parentDir(subDir string) string {
    if(subDir == "/"){
        return "/"
    }else{
        return subDir[:1+strings.LastIndex(subDir[:len(subDir)-1], "/")]
    }
}

// grabs the current directory
func currentDir(subDir string) string {
    if(subDir == "/"){
        return "/"
    }else{
        return subDir[:1+strings.LastIndex(subDir[:len(subDir)], "/")]
    }
}

// adds a folder
func addFolder(path string){
  // if it doesn't end with a / add it
  if(path[len(path)-1:] != "/"){
    path += "/"
  }
  log.Println("Adding to db: ", path)
  dirExistance := dirExistanceCheck(path)// check if dir already exists
  if(dirExistance == true){// if it does, do nothing
    return
  }else{// if it doesn't
    // create empty dir object
    emptyDir := dirData{}
    // encode it and set it to badger
    emptyDirByte := encodeToBytes(emptyDir)
    err := badgerSET(path, emptyDirByte, DBLoc)
    if(err != nil){
      log.Fatal("setting badger dir failed:", err)
    }
    // grab parent dir
    parentDirByte, err := badgerGET(parentDir(selected), DBLoc)
    if(err != nil){
      log.Fatal("getting badger dir failed:", err)
    }
    daParentDir := decodeToDirData(parentDirByte)
    // add the dir path you just made to the subdirs of parent dirData
    Files := daParentDir.Files
    subDirs := daParentDir.SubDirs
    if(subDirs == nil){
      subDirs = []string{path}
    }else{
      subDirs = append(subDirs, path)
    }
    daParentDir.SubDirs = subDirs
    daParentDir.Files = Files
    // then encode that and set it back to badger
    parentDirByte = encodeToBytes(daParentDir)
    err = badgerSET(parentDir(selected), parentDirByte, DBLoc)
    if(err != nil){
      log.Fatal("setting badger dir failed:", err)
    }
  }
  update()
}
