package main

// a thingy for extending widets

import (
    "log"
    "fmt"
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/widget"
    "fyne.io/fyne/v2/dialog"
    "fyne.io/fyne/v2/container"
    // "fyne.io/fyne/v2/canvas"
    // "image/color"

    // "strings"
)

// -------------------------
// This section is for the right file button
type rightFileButton struct {
    widget.Button
    myWindow fyne.Window
    path string
}
func (m *rightFileButton) Tapped(*fyne.PointEvent) {
  log.Println(selected)
  selected = m.path
  fmt.Println(m.path, "was pressed")
  update()
}
func (m *rightFileButton) TappedSecondary(e *fyne.PointEvent) {
  menuItem1 := fyne.NewMenuItem("log aloha", func() { log.Println("aloha") })
  menu := fyne.NewMenu("File", menuItem1)
  popUpMenu := widget.NewPopUpMenu(menu, m.myWindow.Canvas())
  popUpMenu.ShowAtPosition(e.AbsolutePosition)
  popUpMenu.Show()
  log.Println("right clicked")
}
func newRightFileButton(path string) *rightFileButton {
    ret := &rightFileButton{
      myWindow: myWindow,
      path: path,
    }
    ret.ExtendBaseWidget(ret)
    ret.Text = fileName(path)
    return ret
}
// -------------------------
// This section is for the add content button
type addContentButton struct {
    widget.Button
    myWindow fyne.Window
}
func (m *addContentButton) Tapped(e *fyne.PointEvent) {
  menuItem1 := fyne.NewMenuItem("Add Folder", func() {
    // asks for user input and if it's a sring make a folder in current dir
    input := widget.NewEntry()
  	input.SetPlaceHolder("Enter text...")
    // now show the popup
    toShow := widget.NewPopUp(container.NewVBox(input, widget.NewButton("Add Folder", func() {
  		addFolder(parentDir(selected) + input.Text)
  	})), myWindow.Canvas())
    // resize it
    toShow.Resize(fyne.NewSize(150,80))
    // set it to the cursor location and show
    toShow.ShowAtPosition(e.AbsolutePosition)
    toShow.Show()
  })
  menuItem2 := fyne.NewMenuItem("Upload File", func() {
    // call chose file window
    dialog.ShowFileOpen(func(file fyne.URIReadCloser, fileErr error){
      if fileErr != nil {
        // handle error
        return
      }
      if file != nil {
        addFile(file)
        // now update UI
        update()
      }
    }, myWindow)
  })
  menu := fyne.NewMenu("Upload Contents", menuItem1, menuItem2)
  popUpMenu := widget.NewPopUpMenu(menu, m.myWindow.Canvas())
  popUpMenu.ShowAtPosition(e.AbsolutePosition)
  popUpMenu.Show()

}
func newAddContentButton() *addContentButton {
    ret := &addContentButton{
      myWindow: myWindow,
    }
    ret.ExtendBaseWidget(ret)
    ret.Text = "+-Add File-+"
    return ret
}
