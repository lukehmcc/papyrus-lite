package main

// This is the main script for papyrus-lite; papyrus lite is a super basic
// interface for skynet that will be repurposed for the mainline papyrus project

// do the imports
import (
    "log"
    // "log"
    // "strings"
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/app"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/widget"
    "fyne.io/fyne/v2/theme"
    "fyne.io/fyne/v2/layout"
    // "fyne.io/fyne/v2/dialog"
    "fyne.io/fyne/v2/canvas"
    "image/color"
)

// global var for string selected
var selected string
var DBLoc string
var leftContainer *fyne.Container
var rightContainer *fyne.Container
var myWindow fyne.Window

// inits the function
func main(){
    // okay so the plan is to have two windows and a toolbar at the top
    // the left window is the "high level directory" and the right one is the
    // "lower level directory." The base state is root("/") dir on the left,
    // and the right one empty

	// so lets build a basic loyout
  myApp := app.New()
	myWindow = myApp.NewWindow("Papyrus-lite")
  DBLoc = "./PapyrusDB" // define db location
  badgerHomeCheck(DBLoc) // check for the home dir to exist and if it doesn't, it'll init it

	// so define current dir
	selected = "/"

    // now draw
	myWindow.SetContent(draw(myWindow))
	myWindow.Resize(fyne.NewSize(900, 500))
	myWindow.ShowAndRun()
}

// redraws the screen
func draw(myWindow fyne.Window) *fyne.Container{
    leftContainer = container.NewVBox()
    leftContainerScroll := container.NewVScroll(leftContainer)
    rightContainer = container.NewVBox()
    rightContainerScroll := container.NewVScroll(rightContainer)
    // now set them to a layout thing
    mainWindow := container.NewAdaptiveGrid(
    	2,
    	leftContainerScroll,
    	rightContainerScroll,
    )
    // I might add some more logic for the toolbar, but for now it's whatever
    toolbar := widget.NewToolbar(
  		widget.NewToolbarAction(theme.NavigateBackIcon(), func() {
  			selected = parentDir(selected)
        update()
  		}),
  	)
  	content := fyne.NewContainerWithLayout(layout.NewBorderLayout(toolbar, nil, nil, nil), toolbar, mainWindow)
    // Now pass both colums to the update function
    update()
    // also return to be drawn
    return content
}

// add content and update both columns
func update(){
  //on load both of the colums will be cleared
  leftContainer.Objects = leftContainer.Objects[:0]
  rightContainer.Objects = rightContainer.Objects[:0]

  // left container has highlighted selected file/folder
  // also if a the selected thing is a file, grab the parent dir
  currentFile := (selected[len(selected)-1:] != "/") // bool for if it's a file or folder
  var folderByte []byte
  var err error
  // ----- LEFT -----
  // grab the current directory, whether that is from a file or home
  if(currentFile || selected == "/"){
    folderByte, err = badgerGET(currentDir(selected), DBLoc) // grab that folder
    if err != nil {
      log.Printf("unable to fetch from db: %s", err)
      selected = "/" // if it fails to fetch the dir just reset to the home dir
      update()
    }
  // if it is a path that isn't home, grab the parent dir instead
  }else{
    folderByte, err = badgerGET(parentDir(selected), DBLoc) // grab that folder
    if err != nil {
      log.Printf("unable to fetch from db: %s", err)
      selected = "/" // if it fails to fetch the dir just reset to the home dir
      update()
    }
  }

  daFolder := decodeToDirData(folderByte)
  // now since no matter what the left should show the parent dir, show the parent dir
  // add upload button
  leftContainer.Add(newAddContentButton())
  // now list folders
  log.Println("right column")
  log.Println(daFolder.SubDirs)
  for i := 0; i < len(daFolder.SubDirs); i++{
  	thisIndex := daFolder.SubDirs[i]
  	leftContainer.Add(newRightFileButton(thisIndex))
  }
  // and list files
  log.Println(daFolder.Files)
  for i := 0; i < len(daFolder.Files); i++{
		thisIndex := daFolder.Files[i]
		leftContainer.Add(newRightFileButton(thisIndex))
  }

  // ----- RIGHT -----
  // if it's / then don't display anything on the right
  if(selected == "/"){

  // if it's a file, display the file name and a download button on the right
  } else if(currentFile){
    rightContainer.Add(canvas.NewText(selected, color.White))
  // if it's a directory then dispolay the directory contents and an upload button on the right
  }else{
    rightContainer.Add(newAddContentButton()) // can't forget our old add content button
    // grab the directory in byte form and decode it
    folderByte, err = badgerGET(selected, DBLoc) // grab that folder
    if err != nil {
      log.Printf("unable to fetch from db: %s", err)
      selected = "/" // if it fails to fetch the dir just reset to the home dir
      update()
    }
    daFolder = decodeToDirData(folderByte)// decode
    // now list folders
    log.Println("left column")
    log.Println(daFolder.SubDirs)
    for i := 0; i < len(daFolder.SubDirs); i++{
    	thisIndex := daFolder.SubDirs[i]
    	rightContainer.Add(newRightFileButton(thisIndex))
    }
    // and list files
    log.Println(daFolder.Files)
    for i := 0; i < len(daFolder.Files); i++{
  		thisIndex := daFolder.Files[i]
      log.Println("adding", thisIndex, "to", selected)
  		rightContainer.Add(newRightFileButton(thisIndex))
    }
  }
  // now refresh bruh
  leftContainer.Refresh()
  rightContainer.Refresh()
}
