package main

// This package basically hanldes skynet funcitonality

import (
    skynet "github.com/NebulousLabs/go-skynet/v2"
    "fyne.io/fyne/v2"
    "log"
)

// create a client
var client = skynet.New()

// uploads uri to skynet and updates db3

// uploads URI to skynet and return skylink
func uploadURIToSkynet(URI string) (string, error){
	// upload
	skylink, err := client.UploadFile(URI, skynet.DefaultUploadOptions)
	if err != nil {
        return "", err
	}
	log.Printf("Upload successful, skylink: %v\n", skylink)
    return skylink, err
}

// download
func downloadFromSkynet(skylink string, dest string) {
    // download
    err := client.DownloadFile(dest, skylink, skynet.DefaultDownloadOptions)
    if err != nil {
        panic("Something went wrong, please try again.\nError: " + err.Error())
    }
    log.Println("Download successful")
}

// uploads a file to skynet, then sets it in Badger
// take input file and upload to Skynet
func addFile(file fyne.URIReadCloser){
    // upload file to skynet
    skylink, err := uploadURIToSkynet(file.URI().String()[5:])
    if err != nil{
        log.Println("uplaod failed: ", err)
        return
    }
    // create a file object with that skyfile
    var newFile filez
    newFile.Skylink = skylink
    // encode that to byte
    newFileByte := encodeToBytes(newFile)
    // create a db entry for that Objects
    daFileName := (currentDir(selected) + fileName(file.URI().String()))
    log.Println("adding file to", currentDir(selected) + fileName(file.URI().String()))
    err = badgerSET(daFileName, newFileByte, DBLoc)
    if err != nil {
        log.Println("setting file failed:", err)
        return
    }
    // then add that db key to the folder list of key's
    directoryByte, err := badgerGET(currentDir(selected), DBLoc)
    directory := decodeToDirData(directoryByte)
    // now check if any files exist, and if they don't create a slice
    if len(directory.Files) == 0 {
        filesTemp := []string{daFileName}
        directory.Files = filesTemp
    // if the slice does exist, just append
    }else{
        directory.Files = append(directory.Files, daFileName)
    }
    // now re-encode to byte
    newDirectoryByte := encodeToBytes(directory)
    // send it back to the db
    err = badgerSET(currentDir(selected), newDirectoryByte, DBLoc)
    if err != nil {
        log.Println("setting directory failed:", err)
        return
    }
}
