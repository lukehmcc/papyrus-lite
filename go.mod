module gitlab.com/lukehmcc/papyrus-lite

go 1.15

require (
	fyne.io/fyne v1.4.3
	fyne.io/fyne/v2 v2.0.1
	github.com/NebulousLabs/go-skynet/v2 v2.0.1
	github.com/dgraph-io/badger/v3 v3.2011.1
)
